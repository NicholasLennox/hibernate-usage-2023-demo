package no.accelerate.hibernteptelectricboogaloo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HiberntePtElectricBoogalooApplication {

    public static void main(String[] args) {
        SpringApplication.run(HiberntePtElectricBoogalooApplication.class, args);
    }

}
