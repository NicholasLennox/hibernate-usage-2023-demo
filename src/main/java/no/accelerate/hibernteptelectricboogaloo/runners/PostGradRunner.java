package no.accelerate.hibernteptelectricboogaloo.runners;

import jakarta.persistence.EntityManager;
import jakarta.transaction.Transactional;
import no.accelerate.hibernteptelectricboogaloo.services.student.StudentService;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
public class PostGradRunner implements CommandLineRunner {

    private final StudentService studentService;

    public PostGradRunner(StudentService studentService) {
        this.studentService = studentService;
    }

    @Override
    public void run(String... args) throws Exception {

    }


}
