package no.accelerate.hibernteptelectricboogaloo.models;

import jakarta.persistence.*;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.Set;

@Entity
@Getter
@Setter
public class Professor {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column(length = 50, nullable = false)
    private String name;

    @OneToMany(mappedBy = "professor")
    private Set<Student> students;
    @OneToMany(mappedBy = "professor")
    private Set<Subject> subjects;
}
