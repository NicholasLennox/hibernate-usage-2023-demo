package no.accelerate.hibernteptelectricboogaloo.repositories;

import no.accelerate.hibernteptelectricboogaloo.models.Student;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface StudentRepository extends JpaRepository<Student, Integer> {
    @Query(value = "SELECT Id, Name, professor_id FROM Student s WHERE s.name LIKE %?%",
            nativeQuery = true)
    Optional<List<Student>> findByNameContaining(String name);

    @Modifying
    @Query("update Student s set s.professor.id = ?2 where s.id = ?1")
    int updateStudentsProfessorById(int studentId, int professorId);
}
