package no.accelerate.hibernteptelectricboogaloo.repositories;

import no.accelerate.hibernteptelectricboogaloo.models.Professor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProfessorRepository extends JpaRepository<Professor, Integer> {
}
