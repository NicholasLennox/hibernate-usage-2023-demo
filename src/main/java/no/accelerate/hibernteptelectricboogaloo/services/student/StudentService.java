package no.accelerate.hibernteptelectricboogaloo.services.student;

import no.accelerate.hibernteptelectricboogaloo.models.Student;
import no.accelerate.hibernteptelectricboogaloo.services.CRUDService;

public interface StudentService extends CRUDService<Student, Integer> {
    // Extra business logic
    void updateProfessor(int studentId, int professorId);
}