package no.accelerate.hibernteptelectricboogaloo.services.student;

import jakarta.transaction.Transactional;
import no.accelerate.hibernteptelectricboogaloo.models.Student;
import no.accelerate.hibernteptelectricboogaloo.repositories.StudentRepository;
import org.springframework.stereotype.Service;

import java.util.Collection;

/**
 * Services are responsible for the business logic.
 * This involves anything we need to do to meet the functionality
 * that doesn't involve database access directly (repositories do that).
 * There isn't much in the scale of the apps we make, but the delete logic is an exmaple.
 */
@Service
public class StudentServiceImpl implements StudentService {

    private final StudentRepository studentRepository;

    public StudentServiceImpl(StudentRepository studentRepository) {
        this.studentRepository = studentRepository;
    }

    @Override
    public Student findById(Integer id) {
        return studentRepository.findById(id).get();
    }

    @Override
    public Collection<Student> findAll() {
        return studentRepository.findAll();
    }

    @Override
    public Student add(Student entity) {
        return studentRepository.save(entity);
    }

    @Override
    public Student update(Student entity) {
        return studentRepository.save(entity);
    }

    @Override
    @Transactional
    public void deleteById(Integer id) {
        if(studentRepository.existsById(id)) {
            Student stud = studentRepository.findById(id).get();
            stud.setProfessor(null);
            stud.getProject().setStudent(null);
            studentRepository.delete(stud);
        }
    }

    @Override
    @Transactional
    public void delete(Student entity) {
        if(studentRepository.existsById(entity.getId())) {
            Student stud = studentRepository.findById(entity.getId()).get();
            stud.setProfessor(null);
            stud.getProject().setStudent(null);
            studentRepository.delete(stud);
        }
    }

    @Override
    @Transactional
    public void updateProfessor(int studentId, int professorId) {
        studentRepository.updateStudentsProfessorById(studentId,professorId);
    }
}
